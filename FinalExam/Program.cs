﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalExam
{
    class Program
    {
        ProductManagement prManagement = new ProductManagement();

        public static void Main(string[] args)
        {
            Program program = new Program();
            program.Menu();
        }

        static void PressAnyKey()
        {
            Console.WriteLine("\n\t\tEnter any key to continue ... ");
            Console.ReadLine();
            Console.Clear();
        }

        public void Menu()
        {
            int key;
            string answer;
            while (true)
            {
                Console.WriteLine("\n\n\t\t ------------- PRODUCT MANAGEMENT SYSTEM -------------");
                Console.WriteLine("\t\t|-----------------------------------------------------|");
                Console.WriteLine("\t\t|               1. Add product records                |");
                Console.WriteLine("\t\t|               2. Display product records            |");
                Console.WriteLine("\t\t|               3. Delete product by Id               |");
                Console.WriteLine("\t\t|               4. Exit                               |");
                Console.WriteLine("\t\t =====================================================");
                try
                {
                    Console.Write("\n\t #Enter your choice : ");
                    key = Convert.ToInt32(Console.ReadLine());

                    switch (key)
                    {
                        case 1:
                            Console.WriteLine("\n\t\t| Add Product |\n");
                            prManagement.CreateProduct();
                            Console.Write("\t\tDo you want to continue ? (Y/N) :");
                            answer = Convert.ToString(Console.ReadLine());
                            if (answer == "Y" || answer == "y")
                            {
                                Console.Clear();
                                goto case 1;
                            }
                            else if (answer == "N" || answer == "n")
                            {
                                Console.Clear();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\n\t\tYour enter is invalid == Auto Exit ! !\n\n");
                            }
                            break;

                        case 2:
                            if (prManagement.CountProduct() > 0)
                            {
                                Console.WriteLine("\n\t\t| Display Product List |\n");
                                prManagement.ShowProduct(prManagement.GetProducts());
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\n\t\t List Products is null ! \n\n");
                            }
                            PressAnyKey();
                            break;

                        case 3:
                            if (prManagement.CountProduct() > 0)
                            {
                                string id;
                                Console.WriteLine("\n\t\t| Delete Product By ID |\n");
                                Console.Write("\nEnter Product ID : ");
                                id = Console.ReadLine();
                                if (prManagement.DeleteByID(id))
                                {
                                    Console.WriteLine("\nProduct ID = {0} has been deleted ! \n\n", id);
                                }
                                else
                                {
                                    Console.WriteLine("\nProduct might be deleted or not existed !\n\n");
                                }
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\n\t\tList Products is null !\n\n");
                            }
                            Console.Write("\t\tDo you want to continue ? (Y/N) :");
                            answer = Convert.ToString(Console.ReadLine());
                            if (answer == "Y" || answer == "y")
                            {
                                Console.Clear();
                                goto case 3;
                            }
                            else if (answer == "N" || answer == "n")
                            {
                                Console.Clear();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\n\t\tYour enter is invalid == Auto Exit ! !\n\n");
                            }
                            break;

                        case 4:
                            Console.WriteLine("\n\n\t\t \u263A \u263A \u263A Goodbye !! \u263A \u263A \u263A\n\n");
                            Environment.Exit(0);
                            break;

                        default:
                            Console.Clear();
                            Console.WriteLine("\n\t\t\t Function is invalid ! Please re-enter !");
                            break;

                    }

                }
                catch(Exception error)
                {
                    Console.Clear();
                    Console.WriteLine("\n\t\t\t Data type is invalid !!");
                    Console.WriteLine("\t\t\t=> " + error.Message);
                }
            }
        }

    }
}
