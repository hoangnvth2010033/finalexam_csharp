﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalExam
{
    public class ProductManagement
    {
        public List<Product> ListProduct = null;

        //=================== Product Management ===============
        public ProductManagement()
        {
            ListProduct = new List<Product>();
        }

        //Ham tao STT tang dan cho cau hoi
        public int GenerateID()
        {
            int max = 1;
            if(ListProduct != null && ListProduct.Count > 0)
            {
                max = ListProduct[0].STT;
                foreach(Product pr in ListProduct)
                {
                    if (max < pr.STT)
                    {
                        max = pr.STT;
                    }
                }
                max++;
            }
            return max;
        }

        //Ham dem so San pham
        public int CountProduct()
        {
            int count = 0;
            if(ListProduct != null)
            {
                count = ListProduct.Count;
            }
            return count;
        }

        //Ham tim theo ma SP
        public Product FindByProductID(string id)
        {
            Product searchID = null;
            if(ListProduct != null && ListProduct.Count > 0)
            {
                foreach(Product pro in ListProduct)
                {
                    if (pro.ProductID == id)
                    {
                        searchID = pro;
                    }
                }
            }
            return searchID;
        }

        // Ham them moi san pham
        public void CreateProduct()
        {
            Product pr = new Product();
            pr.STT = GenerateID();

            Console.Write("Enter Product ID : ");
            pr.ProductID = Convert.ToString(Console.ReadLine());

            Console.Write("Enter Product Name : ");
            pr.Name = Convert.ToString(Console.ReadLine());

            Console.Write("Enter Product Price : ");
            pr.Price = Convert.ToDouble(Console.ReadLine());

            Console.Write("\n\n\t\t\tDo you want to save this Product ? (Y/N) : ");
            string save = Convert.ToString(Console.ReadLine());
            if (save == "Y" || save == "y" || save == "yes" || save == "YES")
            {
                Console.Clear();
                ListProduct.Add(pr);
                Console.WriteLine("\n\n\t\tAdd Product success !\n");
            }
            else if (save == "N" || save == "n" || save == "no" || save == "NO")
            {
                Console.Clear();
                Console.WriteLine("\n\n\t\t\t Product is not saved !\n\n");
            }
            else
            {
                Console.Clear();
                Console.WriteLine("\n\n\t\t\t Auto break Create Product !\n\n");
            }
        }

        //Ham xoa Product theo ID
        public bool DeleteByID(string id)
        {
            bool IsDeleted = false;
            Product prID = FindByProductID(id);
            if(prID != null)
            {
                IsDeleted = ListProduct.Remove(prID);
            }
            return IsDeleted;
        }

        // Ham hien thi danh sach Product
        public void ShowProduct(List<Product> products)
        {
            Console.WriteLine("{0,-5} | {1,-15} | {2,-15} | {3,-15}  |", "No.", "ProductID", "Product Name", "Price");

            if(products != null && products.Count > 0)
            {
                foreach(Product pr in products)
                {
                    Console.WriteLine("{0,-5} | {1,-15} | {2,-15} | ${3,-15} |", pr.STT, pr.ProductID, pr.Name, pr.Price);

                }
            }
            Console.WriteLine();
        }

        //Ham tra ve danh sach Product
        public List<Product> GetProducts()
        {
            return ListProduct;
        }

    }
}
